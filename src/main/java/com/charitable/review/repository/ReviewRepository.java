package com.charitable.review.repository;

import com.charitable.review.core.Reviewer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReviewRepository extends CrudRepository<Reviewer, Long> {
    List<Reviewer> findByName(String name);
}