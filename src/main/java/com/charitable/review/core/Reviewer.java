package com.charitable.review.core;

import javax.persistence.*;

@Entity
@Table(name = "review")
public class Reviewer {
    private String name;
    private String headline;
    private String body;
    private Integer star;

    @Id
    @Column(name="id_review")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public Reviewer() {

    }

    public Reviewer(String name, String headline, Integer star, String body) {
        this.name = name;
        this.headline = headline;
        this.star = star;
        this.body = body;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
