package com.charitable.review.service;

import com.charitable.review.core.Reviewer;

import java.util.List;

public interface ReviewService {
    public void addReviewer(Reviewer reviewer);
    public boolean checkReviewExist(Reviewer review);
    public List<Reviewer> getAllReviewer();
    public Reviewer getReviewer(Long id);
}
