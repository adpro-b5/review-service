package com.charitable.review.service;

import com.charitable.review.core.Reviewer;
import com.charitable.review.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;

    public void addReviewer(Reviewer reviewer) {
        if (checkReviewExist(reviewer)) return;
        reviewRepository.save(reviewer);
    }
    public boolean checkReviewExist(Reviewer review) {
        List<Reviewer> existing = reviewRepository.findByName(review.getName());
        return !existing.isEmpty();
    }
    public List<Reviewer> getAllReviewer() {
        Iterable<Reviewer> iterable = reviewRepository.findAll();
        List<Reviewer> result = new ArrayList<>();
        iterable.forEach(result::add);
        return result;
    }

    public Reviewer getReviewer(Long id) {
        return reviewRepository.findById(id).get();
    }

}
