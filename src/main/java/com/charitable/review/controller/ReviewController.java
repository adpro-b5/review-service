package com.charitable.review.controller;

import com.charitable.review.core.Reviewer;
import com.charitable.review.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/review")
public class ReviewController {

    @Autowired
    private ReviewService service;

    @GetMapping(path = "")
    public List<Reviewer> displayReview() {
        return service.getAllReviewer();
    }

    @GetMapping(path = "/add")
    public Reviewer getFormAddReview() {
        return new Reviewer();
    }

    @PostMapping(path = "/add-reviewer")
    public void addReview(@RequestBody Reviewer reviewer) {
        service.addReviewer(reviewer);
    }

    @GetMapping(path = "/detail/{idReview}")
    public Reviewer viewReview(@PathVariable(value = "idReview") Long idReview) {
        return service.getReviewer(idReview);
    }

}
